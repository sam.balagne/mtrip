require File.expand_path "spec_helper.rb", __dir__
require File.expand_path "../lib/mtrip.rb", __dir__

describe "Mtrip" do
  let(:mtrip) { Mtrip }
  let(:file) { { "Input.txt" => "Randy", "Code.py" => "Stan", "Output.txt" => "Randy" } }

  it "Returns a hash containing an array of file names for each owner name, in any order" do
    response = { "Randy" => ["Input.txt", "Output.txt"], "Stan" => ["Code.py"] }
    expect(mtrip.group_by_owner(file)).to eq(response)
  end

  it "Checks if it is a palindrome" do
    text_1 = "Deleveled"
    text_2 = "samuel"
    expect(mtrip.is_palindrome(text_1)).to be true
    expect(mtrip.is_palindrome(text_2)).to be false
  end
end
