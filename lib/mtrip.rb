class Mtrip
  def self.group_by_owner(data)
    grouped = data.group_by { |k, v| v }
    flattened = grouped.transform_values { |value| value.flatten }
    flattened.each { |k, v| v.delete_if { |a| a == k } }
  end

  def self.is_palindrome(text)
    text.downcase.reverse == text.downcase
  end
end
