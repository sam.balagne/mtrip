require "net/http"
require "date"
require "uri"
require "json"

class PoiRequest
  URL = "https://www.pois.com/pois"

  def self.fetch
    uri = URI(URL)
    res = Net::HTTP.get_response(uri)
    if res.kind_of? Net::HTTPSuccess
      body = JSON.parse res.body
      body.each do |data|
        MyDatabase.create(id: data["id"], name: data["name"], description: data["description"],
                          rating: data["rating"], location: data["location"], pictures: data["pictures"])
      end
    else
      puts "Error"
    end
  end

  def self.daily_fetch
    yesterday = Date.today - 1
    since_url = URL + "?since=" + yesterday.to_s
    uri = URI(since_url)
    res = Net::HTTP.get_response(uri)
    if res.kind_of? Net::HTTPSuccess
      body = JSON.parse res.body
      body.each do |data|
        MyDatabase.create(id: data["id"], name: data["name"], description: data["description"],
                          rating: data["rating"], location: data["location"], pictures: data["pictures"])
      end
    else
      puts "Error"
    end
  end
end
