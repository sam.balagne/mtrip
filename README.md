## Answers for question 1 group_by_owners and 2 palindrome are in the lib and spec folders

## Question 3 ContactsMigration

### The correct answers are :

- The contacts.name field may be null.

- The table called contacts contains four different data types.

ps: The answers were already given

## Question 4 Employees

### The correct answers are :

- Employee.find_by(name: "John")
- Employee.where(name: "John").first


## Question 5  Dog controller

### The correct answer is :

- match '/person', :to => 'dog#show'


## Question 6 PostsController,

### The correct answer is :

- app/views/posts

## Question 7 validation method

### The correct answers are :

- Time's up!
- validates_form_of
- Validates_uniqueness_of 

ps : I don't know if it is a typo as answer 2 and 3 should be respectively validates_format_of and validates_uniqueness_of instead


## Question 8 randomize

### The correct answer is :

- shuffle


## Question 9 variable x

### The correct answer is :

- [4]


## Question 10  Bar class

### The correct answer are :

- bar = Bar.new(1,2,3)
  bar.total # => 6

- bar = Bar.new(1,2,3,4,5)
  bar.total # => 6


## Question 11  NullOrder

### The correct answer are :

-   class NullOrder
        def price_euro
            0.0
        end
        alias price_usd price_euro
    end

-   class NullOrder
        def method_missing(m, *args, &block)
            m.to_s =~ /price_/ ? 0.0 : super
        end
    end


## Question 12 POI see lib folder 
